/**
 * Configuration class which defines the communication protocol used between
 * Client and Server processes. This is to aid in extensibility of the protocol
 * as well as avoid any minor bugs that arise when using String literals in
 * code.
 *
 * @author Neal B. Beeken
 * @author Anthony G. Musco
 */
public class Protocol {
	
	public static final String SUCCESS = "success";
	public static final String INVALID = "invalid";

	public static final String PUT     = "put";
	public static final String GET     = "get";
	public static final String DEL     = "del";

	public static final String HELP    = "help";
	public static final String BROWSE  = "browse";
	public static final String EXIT    = "exit";
	
	public static final String SERVER  = "server";
	
	public static final String TYPE    = "type";
	public static final String DONE    = "done";
	public static final String LIST	   = "list";
	
	public static final String ENDING  = " \r\n";
	
	public static final String LISTDELIM = ";";
	
	public static final String SIGNAL = "signal";
	
}