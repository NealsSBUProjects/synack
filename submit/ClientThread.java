import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.naming.CommunicationException;

/**
 * Runnable class which operates on the server-side of a client-server
 * connection independent of all other connections.
 *
 */
public class ClientThread implements Runnable {
	
	/**
	 * Socket object that encapsulates the connection to the client.
	 */
	Socket connection;

	
	/**
	 * Constructor which initializes the connection member variable.
	 *
	 * @param connection Socket object which encapsulates the connection
	 *	to the client.
	 */
	public ClientThread(Socket connection) {

		super();
		this.connection = connection;
	
	}

	
	/**
	 * Public setter method for the connection member variable.
	 *
	 * @param connection Socket object which encapsulates the connection
	 *	to the client.
	 */
	public void setConnection(Socket connection) {

		this.connection = connection;

	}


	/**
	 * Handles a client connection by continually accepting commands from the
	 * client and performing the required actions. The process continues until
	 * the client terminates the connection or until an IOException occurs.
	 */
	@Override
	public void run() {

		// Cross fingers and hope nothing goes wrong.
		try {

			// Input buffer bound to the Client connection.
			BufferedReader inFromClient = new BufferedReader(
				new InputStreamReader(connection.getInputStream())
			);

			// Output buffer bound to the Client connection.
			DataOutputStream outToClient = new DataOutputStream(
				connection.getOutputStream()
			);
			
			if(inFromClient == null || outToClient == null) // Nothing can be done in this scenario
				try {
					throw new CommunicationException("NO CONNECTION TO CLIENT");
				} catch (CommunicationException e) {
					System.out.println("NO CONNECTION TO CLIENT");
				}
			
			// MAIN LOOP - Continually process messages from the Client.
			while(connection.isConnected()){			
				

				//System.out.println("Waiting...");
				// Accept the next command and split on spaces.
				String clientCommand = inFromClient.readLine();
				
				if(clientCommand == null){
					break;
				}
				
				//System.out.println("Read: " + clientCommand);
				String[] commandArgs = clientCommand.split(" ");

				// Determine which command was chosen and delegate to the proper handler.
				switch (commandArgs[0].toLowerCase()) {

					case Protocol.PUT:
						Server.handlePut(commandArgs, outToClient);
						break;

					case Protocol.GET:
						Server.handleGet(commandArgs, outToClient);
						break;

					case Protocol.DEL:
						Server.handleDel(commandArgs, outToClient);
						break;

					case Protocol.BROWSE:
						Server.handleBrowse(commandArgs, outToClient);
						break;

					case Protocol.EXIT:
						handleExit(commandArgs, outToClient);
						break;
					// If the command was unrecognized, notify the Client.
					default:
						outToClient.writeBytes(Protocol.INVALID + Protocol.ENDING);
						break;

				}

			}
			
			// Close the connection.
			connection.close(); // line 134 also closes will throw exception

		// If an IO Exception occurs, print the StackTrace.
		} catch (IOException e) {
			
			e.printStackTrace();

		}
		
		
	}


	private void handleExit(String[] commandArgs, DataOutputStream outToClient)
			throws IOException {
		
		outToClient.writeBytes(Protocol.EXIT);
		outToClient.close();
		connection.close(); // Line 116 also closes will throw exception
		
	}

}
