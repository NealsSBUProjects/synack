import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author neal
 *
 */
public class Manager {
	
	/**
	 * Name of the file containing the list of servers to be created when the
	 * Manager class first loads.
	 */
	public static final String CONFIG_FILE = "manager.in";


	/**
	 * Map of TYPE strings that correspond to PORT number values.
	 */
	private static volatile ConcurrentHashMap<String, ServerProcess> runningServers;
	

	/**
	 * Server Manager main method, executes a server for each TYPE in manager.in file
	 * Begins accepting clients while spawning new handling threads for each as they 
	 * arrive. ManagerClientThread invokes methods in this class for management 
	 * functionality
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		
		// Initialize the Map of ServerProcesses.
		runningServers = new ConcurrentHashMap<>();
		
		// Create and initialize a new Server Socket.
		ServerSocket serverSocket = new ServerSocket(2390);
		init(serverSocket);

		// Retrieve the port the server process is running on and display it to
		// the user.
		int listenPort = serverSocket.getLocalPort();
		System.out.println("MANAGER PORT NUMBER: " + listenPort);

		// Load the servers and start them on different Proecesses.
		loadAndRunServers();		
		
		// MAIN LOOP - continually forward connections to DNS servers
		while(true){
			
			System.out.println("Waiting on accept...");
			// Accept a new connection.
			Socket clientSocket = serverSocket.accept();

			// Notify that a new Connection has been created.
			System.out.println("Connection accepted from " + 
				clientSocket.getRemoteSocketAddress());

			//Start Manager Thread for this client
			new Thread(new ManagerClientThread(clientSocket)).start();
			
		}

	}
	
	
	public static void loadAndRunServers() throws IOException, InterruptedException {
		
		// Read in the file contianing the list of server Types.
		File managerFile = new File(CONFIG_FILE);
		ArrayList<String> typeList = new ArrayList<>();

		// If the File could not be found, terminate the Manager.
		if(!managerFile.exists()){

			System.out.println("Did not find 'manager.in' in working directory");
			System.exit(0);

		}else{

			FileInputStream fis = new FileInputStream(managerFile);
			
			//Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		 
		 	// Add all the lines into the typeList.
			String line = null;
			while ((line = br.readLine()) != null) {
				typeList.add(line);
			}
		 
		 	// Close the BufferedReader.
			br.close();

		}

		// Runtime is needed to execute new processes 
		Runtime runtime = Runtime.getRuntime();
		
		// Check three possible locations for the server program based on user's environment
		Process processInSRC = runtime.exec("java -cp src/ Server");
		Process processInBIN = runtime.exec("java -cp bin/ Server");
		Process processInPWD = runtime.exec("java Server");
		
		// Wait for each process 
		// This won't hang our manager as 
		// the execution either fails or "prints USAGE" and exits normally
		int src = processInSRC.waitFor();
		int bin = processInBIN.waitFor();
		int pwd = processInPWD.waitFor();
		
		// Test for which execution was successful and set string accordingly
		String executeCommand = null;
		if(src == 0){
			executeCommand = "java -cp src/ Server ";
		}else if(bin == 0){
			executeCommand = "java -cp bin/ Server ";
		}else if(pwd == 0){
			executeCommand = "java Server ";
		}else{
			System.out.println("Server Program could not be located.");
			System.exit(0);
		}
		
		// For each type : execute a server
		for(String type : typeList){
			
			Process process = runtime.exec(executeCommand + type); // -cp bin/   <-- FOR IDE
			 
			BufferedReader buf = new BufferedReader(
				new InputStreamReader(process.getInputStream())
			);
			
			// Read the port from the server's stdout.
			String portAsString = buf.readLine();
			
			// Parse the port and create a new ServerProcess.
			Integer port = Integer.parseInt(portAsString);
			runningServers.put(type, new ServerProcess(process, port));
			 
		}
		
		// Print out the servers running for user benefit.

		System.out.println(
			"Running Servers:\n" +
			" +--------+---------+\n" +
			" |  TYPE  |  PORT   |\n" +
			" +--------+---------+"
		);

		for(String key : runningServers.keySet()){

			System.out.println(String.format(" |  %5s |  %5d  |", key,
				runningServers.get(key).getPort()));

		}

		System.out.println(" +--------+---------+");

	}

	
	/**
	 * Initializes the indicated socket. Adds a shutdown hook to the Runtime
	 * singleton which closes the indicated socket on shutdown.
	 *
	 * @param s The socket to initialize.
	 */
	private static void init(ServerSocket s){

		// Add a shutdown hook to the Runtime singleton.
		Runtime.getRuntime().addShutdownHook(new Thread(){

			public void run(){

				try {

					s.close();
					System.out.println("\nThe server shut down safely");

				} catch (IOException e){}

			}

		});

	}
	
	/**
	 * Send Client type server's port number .
	 * 
	 * @param commandArgs Command argument array containing the type of
	 *	server the Client wishes to connect to.
	 * @param outToClient DataOutputStream encapulating a write buffer to
	 *	the Manager.
	 *
	 * @throws IOException If something goes wrong.
	 */
	public static void directClientToTypeServer(String[] commandArgs, 
		DataOutputStream outToClient) throws IOException {
		
		// Make sure the formatting is correct.
		if(commandArgs.length != 2) {

			System.out.println("Invalid 'TYPE' formatting.");
			return;

		}

		// Determine which server the user wishes to conenct to.
		String type = commandArgs[1];
		
		// If the indicated type cannot be found, send an INVALID command.
		if(!runningServers.containsKey(type)){

			outToClient.writeBytes(Protocol.INVALID + Protocol.ENDING);

		// Otherwise, return a response indicating the port number of the
		//   requested server.
		}else{

			outToClient.writeBytes(Protocol.SERVER + " " + 
				runningServers.get(type).getPort() + Protocol.ENDING);
		
		}
		
	}



	/**
	 * Lists the currently running servers under the Managers control.
	 * 
	 * @param commandArgs Unused.
	 * @param outToClient DataOutputStream encapulating a write buffer to
	 *	the Manager.
	 * @throws IOException
	 */
	public static void listRunningServers(String[] commandArgs, DataOutputStream
		outToClient) throws IOException {
		
		// Send a delimited string of each server.
		for(String key : runningServers.keySet()){

			outToClient.writeBytes(key + "\t\t" + runningServers.get(key).getPort() 
				+ Protocol.LISTDELIM);

		}

		// End the transmission.
		outToClient.writeBytes(Protocol.ENDING);
		
	}


	
	/**
	 * Prints messages from child server processes to standard out.
	 * 
	 * @param commandArgs
	 * @param outToClient
	 * @throws IOException
	 */
	public static void printFromTypeServer(String[] commandArgs, DataOutputStream outToClient)
			throws IOException {
		
		//SIGNAL = commandArgs[0];
		String type = commandArgs[1];
		
		ServerProcess typeServer = runningServers.get(type);
		
		BufferedReader buf = new BufferedReader(
			new InputStreamReader(typeServer.getProcess().getInputStream())
		);		
				
		String line = buf.readLine();
		
		System.out.println("Message from " + type + " Server: \n\t" + line);
		
	}

}
