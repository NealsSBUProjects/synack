import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Runs a client program that connects to the Domain Name Server via UDP and 
 * performs several tasks. The class runs a program and takes command line 
 * input in the form of HOSTNAME and PORTNUMBER, where HOSTNAME indicates the 
 * domain of the server host and PORTNUMBER specifies the port on which the 
 * process is running.
 *
 * @author Neal B. Beeken
 * @author Anthony G. Musco
 */
public class Client {

	/**
	 * String which tells the user how to properly run the program.
	 */
	private static String USAGE = "usage: java Client HOSTNAME PORTNUMBER\n" 
	 + "HOSTNAME is the machine on which the server program is running\n"
	 + "PORTNUMBER is the listening port of the server machine\n";

	
	/**
	 * Main method for the Client program. The Client class takes two command
	 * line arguments - HOSTNAME and PORTNUMBER (indicating the host and port
	 * on which the server application is running).
	 * 
	 * @param args String arguments passed to the command line. There must be
	 *	exactly TWO arguments for the program to run correctly - HOSTNAME and
	 *  PORTNUMBER.
	 */
	public static void main(String[] args) throws IOException {
		
		String hostname = null;
		Integer serverPort = null;

		// If there are not exactly two arguments, print the usage string for
		// the user and terminate.
		if(args.length != 2){

			// Read in the hostName and port from standard in.
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			System.out.println("No hostname or port number detected.");
			
			// Read hostname.
			System.out.print("Please enter a hostname: ");
			hostname = sc.nextLine();

			// Read port number.
			System.out.print("Please enter a hostname: ");
			serverPort = sc.nextInt(); sc.nextLine(); // Clear buffer.

			// Leave scanner open (it will close System.in).

		} else {
		
			// Grab the HOSTNAME and PORTNUMBER arguments.
			hostname = args[0];
			serverPort = Integer.parseInt(args[1]);
		
		}

		if(hostname == null || serverPort == null) {

			System.out.println("Invalid hostname or server port.");
			System.exit(0);

		}

		// Declare all necessary socket objects and buffers.
		Socket socket = null;
		DataOutputStream outToServer = null;
		BufferedReader inFromServer = null;

		try {

			// Create a socket connecting to the indicated host and port.
			socket = new Socket(hostname, serverPort);

			// Bind the output buffer to the newly created socket.
			outToServer = new DataOutputStream(socket.getOutputStream());

			// Bind the input buffer to the newly created socket.
			inFromServer = new BufferedReader(new InputStreamReader(
				socket.getInputStream())
			);


		// If the host cannot be found, prompt the user and terminate.		
		} catch (UnknownHostException e) {
			
			System.out.println("Invalid hostname");
			System.exit(0);

		// If any IO Exception is encountered, terminate.
		} catch (IOException e) {
		
			System.out.println("IOException on Socket");
			System.exit(0);
		
		}
		
		
		// If the socket or either of the buffers are null, terminate the 
		// program.
		if(outToServer == null || inFromServer == null || socket == null) {

			System.out.println("Failed to Connect");
			System.exit(0);

		}
		
		// Initialize the server socket.
		init(socket);
		
		// Create a Scanner to read user input.
		Scanner sc = new Scanner(System.in);

		// Continue until user enters 'exit'.
		while(true){
			
			// Prompt the user.
			System.out.println("Connected to Server Manager: ");
			printPrompt();
			
			// Receive the user input and split the string on spaces.
			String input = sc.nextLine();
			String[] inputTokens = input.split(" ");

			// Switch on the first command.
			switch (inputTokens[0].toLowerCase()) {
			
				case Protocol.TYPE: {

					// Construct a socket to the requested Server.
					Socket typeSocket = handleType(input, outToServer, 
						inFromServer, hostname);

					// If a connection was made, connect to the server.
					if(typeSocket!=null) {

						connectWithTypeServer(typeSocket, sc, input, 
							outToServer);

					}

				} break;
				
				case Protocol.LIST: {

					// Request a list of records from the server.
					handleList(outToServer, inFromServer);

				} break;

				case Protocol.HELP: {

					// Display the commands to the user.
					printManagerCommands();

				} break;
				
				case Protocol.EXIT: {

					// Disconnnect fromt the server and exit.
					sc.close();
					socket.close();
					System.exit(0);

				} break;
				
				default: {

					System.out.println(input + " is not a valid command. " +
							"Enter \"help\" to see a list of commands");

				} break;
			
			}
			
			
			
			
		}
		
	}


	/**
	 * Initializes the indicated socket. Adds a shutdown hook to the Runtime
	 * singleton which closes the indicated socket on shutdown.
	 *
	 * @param s The socket to initialize.
	 */
	private static void init(Socket socket) {
		
		// Add a shutdown hook to the Runtime singleton.
		Runtime.getRuntime().addShutdownHook(new Thread(){

			public void run(){

				try {

					socket.close();
					System.out.println("\nThe server shut down safely");

				} catch (IOException e){}

			}

		});
		
	}


	/**
	 * Handles a list request from the client reformatting the message to a more 
	 * readable String
	 * 
	 * @param out DataOutputStream which encapsulates a write buffer to the 
	 * 	Server process.
	 * @param in DataOutputStream which encapsulates a read buffer from the 
	 * 	Server process.
	 *
	 * @throws IOException Exception thrown if something goes wrong.
	 */
	private static void handleList(DataOutputStream out, BufferedReader in)
			throws IOException {
		
		// Send List request
		out.writeBytes(Protocol.LIST + Protocol.ENDING);
		
		// Read in list as one entire string
		String list = in.readLine();
		
		// Format it and print to user
		list = list.replace(Protocol.LISTDELIM, Protocol.ENDING);
		System.out.println("Format:");
		System.out.println("SERVERTYPE PORT");
		System.out.println("Server:");
		System.out.println(list.trim());
		System.out.print("\n");
		
	}


	/**
	 * Handles a request to connect to a type server, builds a socket connection
	 * to that server. Returns null if error.
	 *
	 * @param type The type of server 
	 * @param outToServer DataOutputStream encapsulating the writre buffer from
	 *	the Client to the Manager.
	 * @param inFromServer BufferedReader encapsulating the read buffer from
	 *	the Manager process.
	 * @param hostname The name of the host running the Manager the Client is 
	 *	attempting to connect to.
	 *
	 * @return Socket connection to the server of requested type.
	 *
	 * @throws IOException If something goes wrong.
	 */
	private static Socket handleType(String type, DataOutputStream outToServer, 
		BufferedReader inFromServer, String hostname) throws IOException {
		
		// Forward type request to Manager
		outToServer.writeBytes(type + Protocol.ENDING);
		
		// Retrieve the respnse from the server.
		// Should be "server PORT_NUM".
		String response = inFromServer.readLine();

		// If the type was not available in the list of running servers
		if(response.trim().equals(Protocol.INVALID)){
			
			System.out.println("That server is unavailable.");
			return null;
			
		}
		
		System.out.println(response);

		// Obtain port number from response (server PORT_NUM)
		String[] server_port = response.split(" ");
		Integer port = Integer.parseInt(server_port[1]);
		
		// Construct socket with a connection to Type Server
		Socket socket = new Socket(hostname, port);
		
		// Return the socket to the requesting client.
		return socket;
	
	}


	/**
	 * Connects and runs loop to access and manipulate records in a type server.
	 *
	 * @param socket Open socket connection with the Type server in question.
	 * @param sc Reference to an Open Scanner object used for user input.
	 * @param outToServer DataOutputStream encapsulating the writre buffer from
	 *	the Client to the Manager.
	 * @param inFromServer BufferedReader encapsulating the read buffer from
	 *	the Manager process.
	 * @param typecmd The 
	 * @throws IOException
	 */
	private static void connectWithTypeServer(Socket socket, Scanner sc, 
		String typecmd, DataOutputStream toManager)  throws IOException {
		
		DataOutputStream outToServer = null;
		BufferedReader inFromServer = null;
		
		// Bind the output buffer to the newly created socket.
		outToServer = new DataOutputStream(socket.getOutputStream());

		// Bind the input buffer to the newly created socket.
		inFromServer = new BufferedReader(new InputStreamReader(
			socket.getInputStream()));		
		
		String type = typecmd.substring(5).trim();
		
		System.out.println("Connected to "+ type +" Server: ");

		// MAIN LOOP - With type server
		boolean running = true;
		while(running){
			
			// Prompt the user.
			printPrompt();

			// Receive the user input and split the string on spaces.
			String input = sc.nextLine();
			String[] inputTokens = input.split(" ");
			boolean done = false;
			// Switch on the first command.
			switch (inputTokens[0].toLowerCase().trim()) {

				// help
				case Protocol.HELP: {

					printCommands();

				} break;

				// put NAME VALUE TYPE
				case Protocol.PUT: {
				
					handlePut(input, outToServer, inFromServer, type);
				
				} break;

				case Protocol.GET: {
				
					handleGet(input, outToServer, inFromServer, type);
				
				} break;
				
				case Protocol.DEL: {
				
					handleDel(input, outToServer, inFromServer, type);
				
				} break;

				case Protocol.BROWSE: {

					handleBrowse(input, outToServer, inFromServer, type);

				} break;

				case Protocol.DONE: {
					running = false;
					done = true;

				} break;

				case Protocol.EXIT: {

					sc.close();
					socket.close();
					System.exit(0);

				} break;

				default: {

					System.out.println(input + " is not a valid command. " +
						"Enter \"help\" to see a list of commands");

				} break;

			}
			
			if(!done)
				toManager.writeBytes(Protocol.SIGNAL + " " + type + Protocol.ENDING);

		}

	}


	/**
	 * Queries the database for a complete list of all Resource Records.
	 * The method takes an 'input' command String and two buffer references,
	 * 'out' and 'in'.
	 *
	 * @param input The command String indicating record to be queried in the
	 *	database. It must be of format "get NAME TYPE".
	 * 
	 * @param out A DataOutputStream buffer that is bound to the server 
	 *	process.
	 *
	 * @param in A BufferedReader buffer that is bound to the server process.
	 *
	 * @throws IOException Thrown if an IO issue occurs while communicating 
	 *	with the server.
	 */
	private static void handleBrowse(String input, DataOutputStream out, 
		BufferedReader in, String type) throws IOException {

		out.writeBytes(Protocol.BROWSE + Protocol.ENDING);
		
		// Read the entire list as one line
		String list = in.readLine();
		
		// Use the delimiter to create a printable table.
		list = list.replace(Protocol.LISTDELIM, Protocol.ENDING);
		
		// Display
		System.out.print(ResourceRecord.HEADER);
		System.out.print(list);
		System.out.print(ResourceRecord.FOOTER);
		
	}

	
	/**
	 * Queries the database for a ResourceRecord with the indicated NAME TYPE.
	 * If record exists it should be deleted from the database otherwise an
	 * invalid protocol message is expected to be received from the server.
	 * The method takes an 'input' command String and two buffer references,
	 * 'out' and 'in'.
	 *
	 * @param input The command String indicating record to be queried in the
	 *	database. It must be of format "get NAME TYPE".
	 * 
	 * @param out A DataOutputStream buffer that is bound to the server 
	 *	process.
	 *
	 * @param in A BufferedReader buffer that is bound to the server process.
	 *
	 * @throws IOException Thrown if an IO issue occurs while communicating 
	 *	with the server.
	 */
	private static void handleDel(String input, DataOutputStream out, 
		BufferedReader in, String type) throws IOException {
		
		// Make sure that input/output streams are valid.
		if(out == null || in == null) {

			String stream = (out == null) ? "Output" : "Input";
			System.out.println(stream + " stream is null.");
			return;

		}

		// Make sure enough arguments are passed in the input String.
		String[] strArray = input.split(" ");
		if(strArray.length != 2) {

			System.out.println("Invalid 'del' formatting."); 
			return;

		}		

		// Extract the NAME, TYPE arguments from the input string.
		// Ignore     strArray[0].
		String name = strArray[1];
		
		// Write the command "del NAME"
		out.writeBytes(input.trim() + Protocol.ENDING);

		// Receive the response from the server (convert to lower case).
		String reportBack = in.readLine();
		reportBack = reportBack.trim();

		// If the query failed, notify the user.
		if(reportBack.equals(Protocol.INVALID)){

			System.out.println("The record named " + name + ", of type " +  type + 
				" could not be found in the database.");

		// Otherwise, print the information associated with the resource.
		} else {

			System.out.println("Record: " + reportBack + " was deleted.");

		}
		
	}

	
	/**
	 * Queries the database for a ResourceRecord with the indicated NAME TYPE.
	 * The method takes an 'input' command String and two buffer references,
	 * 'out' and 'in'.
	 *
	 * @param input The command String indicating record to be queried in the
	 *	database. It must be of format "get NAME TYPE".
	 * 
	 * @param out A DataOutputStream buffer that is bound to the server 
	 *	process.
	 *
	 * @param in A BufferedReader buffer that is bound to the server process.
	 *
	 * @throws IOException Thrown if an IO issue occurs while communicating 
	 *	with the server.
	 */
	private static void handleGet(String input, DataOutputStream out, 
		BufferedReader in,  String type) throws IOException {


		// Make sure that input/output streams are valid.
		if(out == null || in == null) {

			String stream = (out == null) ? "Output" : "Input";
			System.out.println(stream + " stream is null.");
			return;

		}

		// Make sure enough arguments are passed in the input String.
		String[] strArray = input.split(" ");
		if(strArray.length != 2) {

			System.out.println("Invalid 'GET' formatting."); 
			return;

		}		

		// Extract the NAME, TYPE arguments from the input string.
		// Ignore     strArray[0].
		String name = strArray[1]; 
		
		// Write the command "get NAME TYPE"
		out.writeBytes(input.trim() + Protocol.ENDING);

		// Receive the response from the server (convert to lower case).
		String reportBack = in.readLine();
		reportBack = reportBack.trim();

		// If the query failed, notify the user.
		if(reportBack.equals(Protocol.INVALID)){

			System.out.println("The record named " + name + ", of type " +  type + 
				" could not be found in the database.");

		// Otherwise, print the information associated with the resource.
		} else {

			System.out.println("Record: " + reportBack + " located.");

		}

	}

	
	/**
	 * Sends a NAME, VALUE, TYPE record to the server to store in its database.
	 * The method takes an 'input' command String and two buffer references,
	 * 'out' and 'in'.
	 *
	 * For an type A record, NAME is a domain name, and VALUE is the domain
	 * name's IP address (where TYPE is obviously 'A'). For a type NS record,
	 * NAME is a domain name, and VALUE is the host name of the authoritative
	 * name server that is in charge of the domain name.
	 *
	 * @param input The command String indicating record to be added to the
	 *	database. It must be of format "put NAME VALUE TYPE".
	 * 
	 * @param out A DataOutputStream buffer that is bound to the server 
	 *	process.
	 *
	 * @param in A BufferedReader buffer that is bound to the server process.
	 * @param type 
	 *
	 * @throws IOException Thrown if an IO issue occurs while communicating 
	 *	with the server.
	 */
	private static void handlePut(String input, DataOutputStream out, 
		BufferedReader in, String type) throws IOException {

		// Make sure that input/output streams are valid.
		if(out == null || in == null) {

			String stream = (out == null) ? "Output" : "Input";
			System.out.println(stream + " stream is null.");
			return;

		}// TODO it isn't really possible for these to be null we did checking for that earlier 

		// Make sure enough arguments are passed in the input String.
		String[] strArray = input.split(" ");
		if(strArray.length != 3 ) {

			System.out.println("Invalid 'puts' formatting.");
			return;

		}

		// Extract the NAME, VALUE, TYPE arguments from the input string.
		// Ignore      strArray[0].
		String name  = strArray[1];
		String value = strArray[2];
		// String type  = strArray[3];
		
		// Write the command "put NAME VALUE TYPE"
		out.writeBytes(Protocol.PUT + " " + name + " " + value + " " + type + Protocol.ENDING);
		
		// Receive the response from the server (convert to lower case).
		String reportBack = in.readLine();
		
		reportBack = reportBack.trim();
		
		// Report the success or failure to the user.
		if(reportBack.equals(Protocol.SUCCESS)){

			System.out.println("Successfully PUT record <" + name + ", " + 
				value + ", " + type + ">");

		}else if(reportBack.equals(Protocol.INVALID)){

			System.out.println("Failed to PUT record <" + name + ", " + 
				value + ", " + type + ">");

		}
		
	}


	/**
	 * Convenience method for displaying the prompt to the user.
	 */
	private static void printPrompt() {

		System.out.print("Enter Command > ");
		
	}


	/**
	 * Convenience method for displaying the commands to the user for a given Type Server.
	 */
	private static void printCommands() {
		System.out.println("Command List: \n"
						+  "help \n\t-Prints this command list\n"
						+  "put NAME VALUE \n\t-Adds user defined record to DNS resources\n"
						+  "get NAME \n\t-Returns the value of the record with NAME\n"
						+  "del NAME \n\t-Removes record with NAME\n"
						+  "browse \n\t-Prints all records currently in the database\n"
						+  "done \n\t-Returns user to the manager server\n"
						+  "exit \n\t-Exits this program\n");
		
	}
	
	/**
	 * Convenience method for displaying the commands to the user for the Manager Server.
	 */
	private static void printManagerCommands() {
		System.out.println("Command List: \n"
				+  "type TYPE\n\t-Connects you to the server that manages this resource type.\n"
				+  "list \n\t-Lists all accessible servers\n"
				+  "exit \n\t-Exits this program\n");
		
	}


}
