import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Runnable class that wraps a Socket connection and performs whatever request
 * the client makes to the server.
 *
 * @author Neal B. Beeken
 * @author Anthony G. Musco
 */
public class ManagerClientThread implements Runnable {

	/**
	 * Socket object that encapsulates the connection to the client.
	 */
	Socket connection;
	
	/**
	 * Default constructor which initializes the Socket object wrapped by this
	 * ManagerClientThread.
	 *
	 * @param connection The Socket being wrapped.
	 */
	public ManagerClientThread(Socket connection) {

		super();
		this.connection = connection;

	}

	/**
	 * Primary run function for the ManagerClientThread. This method opens up
	 * the read/write buffers from the Socket connection and continually serves
	 * request made by the client.
	 */
	@Override
	public void run() {
		
		try {

			// Input buffer bound to the Client connection.
			BufferedReader inFromClient = new BufferedReader(
				new InputStreamReader(connection.getInputStream())
			);
			
			// Output buffer bound to the Client connection.
			DataOutputStream outToClient = new DataOutputStream(
				connection.getOutputStream()
			);
		
			// Continually serve the client.
			while(connection.isConnected()){

				// Grab the command from the read buffer.
				String clientCommand = inFromClient.readLine();
				
				// If nothing was read, exit the loop.
				if(clientCommand == null){
					break;
				}
				
				// Notify the user that a command was read from the client.
				System.out.println("READ: " + clientCommand + " from " + 
					connection.getRemoteSocketAddress());

				// Get the commands passed by the client.
				String[] commandArgs = clientCommand.split(" ");
				
				// Delegat the commands based on the argument.
				switch (commandArgs[0]) {

					case Protocol.LIST: {
					
						// List all of the servers currently running.
						Manager.listRunningServers(commandArgs, outToClient);
					
					} break;
					
					case Protocol.TYPE: {
					
						// Print the port the requested server is running on.
						Manager.directClientToTypeServer(commandArgs, outToClient);
					
					} break;
					
					case Protocol.SIGNAL: {

						// Print a message from this server.
						Manager.printFromTypeServer(commandArgs, outToClient);
					
					} break;
					
					default: {

						// Notify that the command was invalid.
						outToClient.writeBytes(Protocol.INVALID + Protocol.ENDING);
					
					} break;

				}

			}
		
		// Nothing should go wrong, but just in case...
		} catch (IOException e) {
			
			e.printStackTrace();
			
		}

	}

}
