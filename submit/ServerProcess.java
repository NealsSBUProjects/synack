/**
 * Convenience class which associates a running Proecess with a specific Port
 * number. This is used by the Manager class to keep track of the individual
 * servers being run.
 *
 * @author Neal B. Beeken
 */
public class ServerProcess {

	/**
	 * The Process being associated.
	 */
	private final Process process;
	
	/**
	 * The Rort number associated with the indicated Process.
	 */
	private final Integer port;
	
	/**
	 * Default constructor which initializes the object with the indicated 
	 * Process reference and Port number.
	 *
	 * @param process The Process being associated with a Port number.
	 * @param port The Port number associated with the inidcated Process.
	 */
	public ServerProcess(Process process, Integer port) {

		super();
		this.process = process;
		this.port = port;
	
	}

	/**
	 * Public getter method for the Process.
	 *
	 * @return The Process for this ServerProcess.
	 */
	public Process getProcess() {
	
		return process;
	
	}

	/**
	 * Public getter method for the Port number.
	 *
	 * @return The Port number for this ServerProcess.
	 */
	public Integer getPort() {
	
		return port;
	
	}
	
}
