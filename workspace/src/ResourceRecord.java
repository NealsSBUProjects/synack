
/**
 * Encapsulates three String data members - NAME, VALUE, and TYPE. This class
 * is used primary as a grouping structure for convenience,
 *
 * @author Neal B. Beeken
 * @author Anthony G. Musco
 */
public class ResourceRecord {

	/**
	 * Header used for printing a neatly formatted table of Resource Records.
	 */
	public static final String HEADER = "\n" +
	"  NAME SERVER RESOURCE RECORDS: \n" +
	" +==============================================================+\n" + 
	" |            NAME           |           VALUE           | TYPE |\n" +
	" +===========================+===========================+======+\n";

	/**
	 * Format string used to print a neatly formatted table of Resource Records.
	 */
	public static final String FORMAT_STRING = " | %-25s | %25s |  %3s |";

	/**
	 * Header used to close a neatly formatted table of Resource Records.
	 */
	public static final String FOOTER = 
	"+===========================+===========================+======+\n";

	/**
	 * The NAME member for this ResourceRecord (serves as the key for mapping).
	 */
	private String name;

	/**
	 * The VALUE member for this ResourceRecord. In A type records, VALUE
	 * represents the domains IP address. In NS type records, VALUE represents
	 * the domains authoritative name server.
	 */
	private String value;

	/**
	 * The TYPE member for this ResourceRecord. This member can be either "A"
	 * or "NS", and effects how NAME and VALUE should be interpreted.
	 */
	private String type;

	
	/**
	 * Constructor which initializes the NAME, VALUE, and TYPE member variables.
	 *
	 * @param name The NAME member for this ResourceRecord.
	 *
	 * @param value The VALUE member for this ResourceRecord.
	 *
	 * @param type the TYPE member for this ResourceRecord.
	 */
	public ResourceRecord(String name, String value, String type) {

		this.value = value;
		this.name  = name;
		this.type  = type;

	}

	
	/**
	 * Constructor which initializes the NAME, VALUE, and TYPE member variables.
	 * This constructor is used primarily for convenience, as it takes in a 
	 * 4-vector of Strings of which the 0th element is ignored.
	 *
	 * @param fields Array of 4 Strings - [cmd, NAME, VALUE, TYPE]. 'cmd' is 
	 *	ignored when constructing the ResourceRecord.
	 */	
	public ResourceRecord( String [] fields , String type){

		// If 'fields' is of proper length, extract the member variable values.
		if(fields.length == 4) {

			// Ignore    fields[0].
			this.name  = fields[1].trim();
			this.value = fields[2].trim();
			this.type  = type;
	
		}

	}

	
	/**
	 * Public getter method for the NAME member variable.
	 *
	 * @return A reference to the NAME member variable.
	 */
	public String getName() {

		return name;

	}

	
	/**
	 * Public setter method for the NAME member variable.
	 *
	 * @param name The new NAME member variable.
	 */
	public void setName(String name) {

		this.name = name;

	}	

	
	/**
	 * Public getter method for the VALUE member variable.
	 *
	 * @return A reference to the VALUE member variable.
	 */
	public String getValue() {

		return value;

	}

	
	/**
	 * Public setter method for the VALUE member variable.
	 *
	 * @param value The new VALUE member variable.
	 */
	public void setValue(String value) {

		this.value = value;

	}

	
	/**
	 * Public getter method for the TYPE member variable.
	 *
	 * @return A reference to the TYPE member variable.
	 */
	public String getType() {

		return type;

	}

	
	/**
	 * Public setter method for the TYPE member variable.
	 *
	 * @param type The new TYPE member variable.
	 */
	public void setType(String type) {

		this.type = type;

	}

	
	/**
	 * Determines if this ResourceRecord has been properly initialized.
	 * 
	 * @param type The type to check against to see if this is a valid 
	 * resource for the calling server
	 * 
	 * @return True if and only if all member variables are null.
	 */
	public boolean isValidRecord(String type){

		boolean isNull = (value == null && name == null && type == null);
		boolean rightType = type.trim().equalsIgnoreCase(this.type.trim());
		
		return !isNull && rightType;

	}

	
	/**
	 * Returns correctly formatted Resource Record as String.
	 *
	 * @return "&gt; NAME, VALUE, TYPE &lt;"
	 */
	@Override
	public String toString() {

		return "< " + name + ", " + type + ", " + value + " >";

	}

	/**
	 * Returns a Resource Record String for printing into a formatted table.
	 *
	 * @return " | NAME  | VALUE | TYPE | "
	 */
	public String tableEntryString() {

		return String.format(FORMAT_STRING, name, value, type);

	}
	
}
