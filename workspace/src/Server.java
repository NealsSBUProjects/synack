import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Primary Server class which acts to accept Client connections and delegate
 * them to independent ClientThread processes. On startup, the Server receives
 * a port number from the OS and begins accepting connections on that port. 
 * when a connection is made, a new ClientThread is created and the server-side
 * socket is forwarded for processing.
 *
 * @author Neal B. Beeken
 * @author Anthony G. Musco
 */
public class Server {
	
	/**
	 * Initial port for the Server process. This is only a temporary value
	 * used until the OS assigns the process a permanent IP address.
	 */
	public static final int EPHEMERAL_PORT = 0;
	
	/**
	 * Default size of the ConcurrentHashMap database.
	 */
	public static final int DEFAULT_DATABASE_SIZE = 1000;

	/**
	 * Database of ResourceRecords. The database maps Strings to ResourceRecord
	 * objects, and is asynchronous to avoid concurrency problems.
	 */
	private static volatile ConcurrentHashMap<String, ResourceRecord> database;
	
	/**
	 * The type of record that this server manages
	 */
	private static String type;
	
	/**
	 * String which tells the user how to properly run the program.
	 */
	private static String USAGE = "Usage: java Server TYPE\n" 
	  + "  TYPE is the server's resource type that it is set to manage\n";
	
	/**
	 * Creates the ResourceRecord database and the Server Socket, and 
	 * continually forwards Client connections to new ClientThreads after
	 * establishing new connections.
	 *
	 * @param args Command line arguments. As of now, they serve no purpose.
	 */
	public static void main(String[] args) throws Exception {
		
		if(args.length != 1){
			
			System.out.println(USAGE);
			System.exit(0);
			
		}
		
		// Determine which type of name server is being run. 
		type = args[0];
		
		// Create and initialize a new Server Socket.
		ServerSocket serverSocket = new ServerSocket(EPHEMERAL_PORT);
		init(serverSocket);

		// Determine which port was assigned to this socket.
		int listenPort = serverSocket.getLocalPort();

		// Socket for communicating with parent process
		System.out.println(listenPort);
		
		// Initialize the database to a default number of records.
		database = new ConcurrentHashMap<>(DEFAULT_DATABASE_SIZE);
		
		// MAIN LOOP - continually forward connections to new ClientThreads.
		while(true){

			// Accept a new connection.
			Socket clientSocket = serverSocket.accept();

			// Forward to new ClientThread.
			new Thread(new ClientThread(clientSocket)).start();	
		
		}

	}

	
	/**
	 * Static method for handling a BROWSE request for one of the ClientThreads.
	 *
	 * @param commandArgs Tokenized list of command arguments provided by the
	 *	Client process.
	 *
	 * @param out DataOutputStream which encapsulates a write buffer to the 
	 * 	Client process.
	 */
	public static void handleBrowse(String[] commandArgs, DataOutputStream out) 
		throws IOException {

		// Access each record and send it with a list delimiter
		int count = 0;
		for(String it : database.keySet()){

			out.writeBytes(database.get(it).tableEntryString() + 
				Protocol.LISTDELIM);
			count++;

		}

		// End the transmission.
		out.writeBytes(Protocol.ENDING);

		// Log how many items were sent to the client.
		System.out.println("BROWSE: Sent " + count + " items.");
		
	}

	
	/**
	 * Static method for handling a DEL request for one of the ClientThreads.
	 *
	 * @param commandArgs Tokenized list of command arguments provided by the
	 *	Client process.
	 *
	 * @param out DataOutputStream which encapsulates a write buffer to the 
	 * 	Client process.
	 */
	public static void handleDel(String[] commandArgs, DataOutputStream out) 
		throws IOException {
		
		// Ensure that the arguments are valid
		if(commandArgs.length != 2) {

			out.writeBytes(Protocol.INVALID + Protocol.ENDING);
			System.out.println("DEL: Incorrect number of arguments.");
			return;

		}
		
		// Ignore     commandArgs[0].
		String name = commandArgs[1];

		// Fetch the ResourceRecord from the database.
		ResourceRecord record = database.remove(name);

		// If the record was not found, return "invalid".
		if(record == null) {

			out.writeBytes(Protocol.INVALID + Protocol.ENDING);
			System.out.println("DEL: No record '" + name + "'' in database");
			return;		

		}

		// Write the record as a string of its member variables in the format:
		// NAME VALUE TYPE
		out.writeBytes(record.toString() + Protocol.ENDING);
		System.out.println("DEL: " + record.toString());

	}


	/**
	 * Static method for handling a GET request for one of the ClientThreads.
	 *
	 * @param commandArgs Tokenized list of command arguments provided by the
	 *	Client process.
	 *
	 * @param out DataOutputStream which encapsulates a write buffer to the 
	 * 	Client process.
	 */
	public static void handleGet(String[] commandArgs, DataOutputStream out) 
		throws IOException {

		// Ensure that the arguments are valid
		if(commandArgs.length != 2) {

			out.writeBytes(Protocol.INVALID + Protocol.ENDING);
			System.out.println("GET: Incorrect number of arguments.");
			return;

		}
		
		// Ignore     commandArgs[0].
		String name = commandArgs[1];

		// Fetch the ResourceRecord from the database.
		ResourceRecord record = database.get(name);

		// If the record was not found, return "invalid".
		if(record == null) {

			out.writeBytes(Protocol.INVALID + Protocol.ENDING);
			System.out.println("GET: No record '" + name + "' in database.");
			return;		

		}

		// Write the record as a string of its member variables in the format:
		// NAME VALUE TYPE
		out.writeBytes(record.toString() + Protocol.ENDING);
		
		// Log that GET was successful.
		System.out.println("GET: " + record.toString());
		
	}


	/**
	 * Static method for handling a PUT request for one of the ClientThreads.
	 *
	 * @param commandArgs Tokenized list of command arguments provided by the
	 *	Client process.
	 *
	 * @param out DataOutputStream which encapsulates a write buffer to the 
	 * 	Client process.
	 */
	public static void handlePut(String[] commandArgs, DataOutputStream out) 
		throws IOException {

		// Input must have at least three Strings in order to create record
		if(commandArgs.length < 3){
			
			out.writeBytes(Protocol.INVALID + Protocol.ENDING);
			return;
		
		}
		
		// Create a new resource record based on the arguments. Must be
		//  formatted [cmd, NAME, VALUE, TYPE]
		ResourceRecord record = new ResourceRecord(commandArgs, type);
		
		// Put the record into the database.
		database.put(record.getName(), record);

		// Tell the client that the PUT request was successful.
		out.writeBytes(Protocol.SUCCESS + Protocol.ENDING);
		
		// Log that the PUT request was successful.
		System.out.println("PUT: " + record.toString());

	}

	
	/**
	 * Initializes the indicated socket. Adds a shutdown hook to the Runtime
	 * singleton which closes the indicated socket on shutdown.
	 *
	 * @param s The socket to initialize.
	 */
	private static void init(ServerSocket s){

		// Add a shutdown hook to the Runtime singleton.
		Runtime.getRuntime().addShutdownHook(new Thread(){

			public void run(){

				try {

					s.close();
					System.out.println("\nThe server shut down safely");

				} catch (IOException e){}

			}

		});

	}

}
